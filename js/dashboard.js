var listDashboard = [
    ["TV", "00:1B:44:11:3A:B7", "127.0.0.2", "2021-05-31", "50"],
    ["Washer", "00:1B:44:11:3A:B8", "127.0.0.3", "2021-05-31", "60"],
    ["Refrigerator", "00:1B:44:11:3A:B9", "127.0.0.4", "2021-05-31", "80"],
    ["Selling Fan", "00:1B:44:11:3A:B2", "127.0.0.5", "2021-05-31", "100"]
];

function insertarrayData() {
    var table = document.getElementById("devicesTable");
    var length = listDashboard.length;
    for(var i=length-1; i>=0; i--) {
        var row = table.insertRow(1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);
        cell1.innerHTML = listDashboard[i][0];
        cell2.innerHTML = listDashboard[i][1];
        cell3.innerHTML = listDashboard[i][2];
        cell4.innerHTML = listDashboard[i][3];
        cell5.innerHTML = listDashboard[i][4];
    }
}

function createChart(array) {
    lb = [];
    pc = [];
    for (var i = 0; i < array.length; i++) {
      lb.push(array[i][0]);
    }
    for (var i = 0; i < array.length; i++) {
      pc.push(array[i][4]);
    }
    const data = {
        labels: lb,
        datasets: [{
          label: 'Power Consumption',
          data: pc,
          backgroundColor: [
            'rgb(255, 99, 132)',
            'rgb(245, 172, 37)',
            'rgb(255, 205, 86)',
            'rgb(54, 162, 235)',
            'rgb(5, 230, 20))',
          ],
          hoverOffset: 4
        }]
      };
    
      const config = {
      type: 'doughnut',
      data: data,
    };
    const myChart = new Chart(
        document.getElementById('myChart'),
        config
    );
}

function loadingPageDashboard() {
    createChart(listDashboard);
    insertarrayData();
}

function addDevice() {
    var name = document.getElementById("name").value;
    var ip = document.getElementById("IP").value;
    var powerConsumption = Math.floor(Math.random() * 101);
    if(name!=""&&ip!="") {
        listDashboard.push([name, '', ip, '', powerConsumption]);
        var table = document.getElementById("devicesTable");
        var row = table.insertRow(1);
        row.insertCell(0).innerHTML = listDashboard[listDashboard.length-1][0];
        row.insertCell(1).innerHTML = "";
        row.insertCell(2).innerHTML = listDashboard[listDashboard.length-1][2];
        row.insertCell(3).innerHTML = "";
        row.insertCell(4).innerHTML = listDashboard[listDashboard.length-1][4];
    }
    else {
        alert("Wrong input!");
    }
}