const listLog = [
    ["101","TV", "Turn On", "124689"],
    ["102","Washer", "Sleep", "124533"],
    ["103","Selling Fan", "Turn Off", "124518"],
    ["104","TV", "Turn On", "124689"],
    ["105","Washer", "Sleep", "124533"],
    ["106","Selling Fan", "Turn Off", "124518"],
    ["107","TV", "Turn On", "124689"],
    ["108","Washer", "Sleep", "124533"],
    ["109","Selling Fan", "Turn Off", "124518"],
    ["110","TV", "Turn On", "124689"],
    ["111","Washer", "Sleep", "124533"],
    ["112","Selling Fan", "Turn Off", "124518"],
    ["113","TV", "Turn On", "124689"],
    ["114","Washer", "Sleep", "124533"],
    ["115","Selling Fan", "Turn Off", "124518"],
    ["116","TV", "Turn On", "124689"],
    ["117","Washer", "Sleep", "124533"],
    ["118","Selling Fan", "Turn Off", "124518"],
    ["119","TV", "Turn On", "124689"],
    ["120","Washer", "Sleep", "124533"],
    ["121","Selling Fan", "Turn Off", "124518"],
    ["122","TV", "Turn On", "124689"],
    ["123","Washer", "Sleep", "124533"],
    ["124","Selling Fan", "Turn Off", "124518"],
    ["125","TV", "Turn On", "124689"],
    ["126","Washer", "Sleep", "124533"],
    ["127","Selling Fan", "Turn Off", "124518"],
    ["128","TV", "Turn On", "124689"],
    ["129","Washer", "Sleep", "124533"],
    ["130","Selling Fan", "Turn Off", "124518"],
];

function loadActionLog() {
    paginateButton(listLog);
    paginate(1, listLog);
}

function paginate(currentPage, arrayData) {
    btns = document.getElementsByClassName("page-btn");
    for(var i=0; i<btns.length; i++) {
        if(i==currentPage-1) {
            btns[i].style.color = "red";
        }
        else btns[i].style.color = "blue";
    }

    var table = document.getElementById("devicesTable");
    for(var i=1; i<table.rows.length; i++) {
        table.deleteRow(i);
        i--;
    }

    var maxLenght = 12;
    var length = arrayData.length;
    var start = (currentPage-1)*maxLenght;
    var end = currentPage*maxLenght;
    if(end > length) {
        end = length;
    }
    for(var i=end-1; i>=start; i--) {
        var row = table.insertRow(1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        cell1.innerHTML = arrayData[i][0];
        cell2.innerHTML = arrayData[i][1];
        cell3.innerHTML = arrayData[i][2];
        cell4.innerHTML = arrayData[i][3];
    }
}
function paginateButton(arrayData) {

    //get number of pages
    var length = arrayData.length; //number of record
    var maxLenght = 12; //number per page
    var numberOfPages = length/maxLenght + 1; //number of page
    if(length%maxLenght==0) {
        numberOfPages = length/maxLenght;
    }
    numberOfPages = parseInt(numberOfPages);

    //Add button pagination
    for(var i=0; i<numberOfPages;i++) {
        let button = document.createElement("button");
        button.className = "page-btn";
        button.id = (i+1);
        button.innerHTML = i+1;
        button.onclick = function() {
            paginate(button.id, arrayData);
        };
        var div = document.getElementById("page");
        div.appendChild(button);
    }
}

function search() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("devicesTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}